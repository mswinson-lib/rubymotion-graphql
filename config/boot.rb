require 'bundler/setup'
require 'sinatra/cross_origin'
require 'graphql/schema/rubymotion'
require 'json'

require './app'
