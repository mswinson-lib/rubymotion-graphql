FROM ruby:latest

ADD . /var/services/rubymotion-graphql
WORKDIR /var/services/rubymotion-graphql

RUN ./scripts/setup
CMD ./scripts/init
