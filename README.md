# rubymotion-graphql

GraphQL interface to Rubymotion toolbox

## Usage

      docker run mswinson/rubymotion-graphql


## Configuration

Ports
  
      5000


## Development

run

    export LOCALHOST=<yourhost>
    docker-compose up

open

    http://LOCALHOST:4000

## Contributing

1. Fork it ( http://bitbucket.org/mswinson-planetdb/rubymotion-graphql/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
