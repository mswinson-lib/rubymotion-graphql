require 'rubocop/rake_task'

namespace :style do
  RuboCop::RakeTask.new(:check) do |t|
    t.options = [
      '--format', 'progress',
      '--format', 'offenses',
      '--format', 'json',
      '--out', "#{REPORTDIR}/style/style.json",
      '--format', 'emacs',
      '--out', "#{TARGETDIR}/errors-ruby.err",
      '--format', 'html',
      '--out', "#{REPORTDIR}/style/index.html"
    ]

    t.fail_on_error = true
  end
end
